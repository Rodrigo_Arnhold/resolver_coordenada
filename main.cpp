#include <iostream>
#include <math.h>
#include "UTM.h"

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

const double RAIO_TERRA_KM		 = 6371.0;
const double DEF_PI				 = 3.14159265358979323846;
const double Circunferencia_Terra = 2 * DEF_PI * RAIO_TERRA_KM;
const double C_Grau				 = Circunferencia_Terra / 360.0;
const double C_Minuto			 = C_Grau / 60.0;
const double C_Segundos			 = C_Grau / 3600.0;




void teste(double x1,double x2,double x3, double y1, double y2, double y3, double d1, double d2, double d3, double *x, double *y  ){
	
	float x_3_2 = x3 -x2; 
	float x_1_3 = x1 -x3;
	float x_2_1 = x2 -x1;
	float y_3_2 = y3 -y2; 
	float y_1_3 = y1 -y3;
	float y_2_1 = y2 -y1;
	
	float A = pow(x1,2) + pow(y1,2) - pow(d1,2);
	float B = pow(x2,2) + pow(y2,2) - pow(d2,2);
	float C = pow(x3,2) + pow(y3,2) - pow(d3,2);
	
	*x = ((A * y_3_2) + (B * y_1_3) + (C * y_2_1 )) / (2*( (x1 * y_3_2) + (x2 * y_1_3) + (x3 * y_2_1)  ));
	
	*y = ((A * x_3_2) + (B * x_1_3) + (C * x_2_1 )) / (2*( (y1 * x_3_2) + (y2 * x_1_3) + (y3 * x_2_1)  ));
	
	//printf("\nx: %f y:%f", x,y);
	
	
}










void converter_DecimalToGrau(double coordenada, unsigned int *grau, unsigned int *min, double *segundos){
	// funcao testada e OK
	coordenada = coordenada > 0 ? coordenada : (coordenada * -1);
	*grau = (unsigned int)coordenada;
	coordenada = coordenada - *grau; 
	*min  = (unsigned int)(coordenada * 60);
	coordenada = (coordenada * 60) - *min;
	*segundos = coordenada * 60 ;
}


double converter_GrausToKilometros(unsigned int grau, unsigned int min, double segundos ){
	
	double Distancia_Km;
	
	Distancia_Km = (( C_Grau * grau ) + (C_Minuto * min) + (C_Segundos * segundos));
	
	return Distancia_Km;
}


void converter_KilometrosToGraus(double Distancia_Km, unsigned int *grau, unsigned int *min, double *segundos ){
	
	// funcao testada e OK
	
	*grau = (unsigned int)Distancia_Km / C_Grau;
	Distancia_Km = Distancia_Km - (*grau * C_Grau);	
	*min = (unsigned int)( Distancia_Km / C_Minuto);
	Distancia_Km = Distancia_Km - (*min * C_Minuto);	
	*segundos = (Distancia_Km / C_Segundos);
}


double converter_GrausToDecimal(unsigned int grau, unsigned int min, double segundos){
	// funcao testada e OK
	
	return (((segundos/60) + min)/60) + grau;	
}


/*********************************************************************//**
* @brief 		Calculate the Distance between two Points - Km.
* @param[in]    P1_DLat		Point 1 - Decimal Degree Latitude.
* @param[in]    P1_DLong	Point 1 - Decimal Degree Longitude.
* @param[in]    P2_DLat		Point 2 - Decimal Degree Latitude.
* @param[in]    P2_DLong	Point 2 - Decimal Degree Longitude.
* @return		Distance	Distance between two points - Km
*********************************************************************/
float fLib_Geo_CalcDistanceBetweenTwoPointsKm( float P1_DLat, float P1_DLong, float P2_DLat, float P2_DLong )
{
	float 	P1_RLat;
	float 	P1_RLong;
	float 	P2_RLat;
	float 	P2_RLong;
	float 	Delta_RLat;
	float 	Delta_RLong;
	float 	Dist_RPartA;
	float 	Dist_RPartB;
	float	Dist_Km;

	/* Convert from "Decimal Degree" to "Radians" */
	/* { Point Radians = Point Decimal * (pi / 180) } */
	P1_RLat  = (P1_DLat  * (3.14159265358979323846 / 180.0)); /* Point 1 - Radians Latitude  */
	P1_RLong = (P1_DLong * (3.14159265358979323846 / 180.0)); /* Point 1 - Radians Longitude */
	P2_RLat  = (P2_DLat  * (3.14159265358979323846 / 180.0)); /* Point 2 - Radians Latitude  */
	P2_RLong = (P2_DLong * (3.14159265358979323846 / 180.0)); /* Point 2 - Radians Longitude */

	/* Calculate the "Delta - Radians" */
	/* { Delta = Point 2 - Point 1 } */
	Delta_RLat  = (P2_RLat  - P1_RLat);  /* Delta - Radians Latitude  */
	Delta_RLong = (P2_RLong - P1_RLong); /* Delta - Radians Longitude */

	/* Calculate the "Distance - Radians" */
	/* { Dist_RPartA = (sen(Delta_RLat / 2) * sen(Delta_RLat / 2)) + cos(P1_RLat) * cos(P2_RLat) * (sen(Delta_RLong / 2) * sen(Delta_RLong / 2)) } */
	Dist_RPartA = ((sin(Delta_RLat / 2) * sin(Delta_RLat / 2)) + cos(P1_RLat) * cos(P2_RLat) * (sin(Delta_RLong / 2) * sin(Delta_RLong / 2)));
	/* { Dist_RPartB = (2 * atan2(square(Dist_RPartA), square(1 - Dist_RPartA))) } */
	Dist_RPartB = (2 * atan2(sqrt(Dist_RPartA), sqrt(1 - Dist_RPartA)));

	/* Convert the Distance to Km */
	/* { Dist_Km = 6371 x Dist_RPartB } "6371Km = Earth Radius" */
	Dist_Km = (6371.0 * Dist_RPartB);

	return Dist_Km;
}


float Calc_CoordenadaMedia(float x1, float y1,float x2, float y2, float d ){
	
	float aux;
	
	return ( pow(x1,2) + pow(y1,2) - pow((x2 - d),2) + pow(y2,2) + pow(d,2) )/ (2 * d);
}


/* circle_circle_intersection() *
 * Determine the points where 2 circles in a common plane intersect.
 *
 * int circle_circle_intersection(
 *                                // center and radius of 1st circle
 *                                double x0, double y0, double r0,
 *                                // center and radius of 2nd circle
 *                                double x1, double y1, double r1,
 *                                // 1st intersection point
 *                                double *xi, double *yi,              
 *                                // 2nd intersection point
 *                                double *xi_prime, double *yi_prime)
 *
 * This is a public domain work. 3/26/2005 Tim Voght
 *
 */
#include <stdio.h>
#include <math.h>

int circle_circle_intersection(double x0, double y0, double r0,
                               double x1, double y1, double r1,
                               double *xi, double *yi,
                               double *xi_prime, double *yi_prime)
{
  double a, dx, dy, d, h, rx, ry;
  double x2, y2;

  /* dx and dy are the vertical and horizontal distances between
   * the circle centers.
   */
  dx = x1 - x0;
  dy = y1 - y0;

  /* Determine the straight-line distance between the centers. */
  //d = sqrt((dy*dy) + (dx*dx));
  d = hypot(dx,dy); // Suggested by Keith Briggs

  /* Check for solvability. */
  if (d > (r0 + r1))
  {
    /* no solution. circles do not intersect. */
    return 0;
  }
  if (d < fabs(r0 - r1))
  {
    /* no solution. one circle is contained in the other */
    return 0;
  }

  /* 'point 2' is the point where the line through the circle
   * intersection points crosses the line between the circle
   * centers.  
   */

  /* Determine the distance from point 0 to point 2. */
  a = ((r0*r0) - (r1*r1) + (d*d)) / (2.0 * d) ;

  /* Determine the coordinates of point 2. */
  x2 = x0 + (dx * a/d);
  y2 = y0 + (dy * a/d);

  /* Determine the distance from point 2 to either of the
   * intersection points.
   */
  h = sqrt((r0*r0) - (a*a));

  /* Now determine the offsets of the intersection points from
   * point 2.
   */
  rx = -dy * (h/d);
  ry = dx * (h/d);

  /* Determine the absolute intersection points. */
  *xi = x2 + rx;
  *xi_prime = x2 - rx;
  *yi = y2 + ry;
  *yi_prime = y2 - ry;

  return 1;
}


void run_test(double x0, double y0, double r0,
              double x1, double y1, double r1)
{
  double x3, y3, x3_prime, y3_prime;
unsigned int grau,min;
	double segundos;
	
  printf("x0=%F, y0=%F, r0=%F, x1=%F, y1=%F, r1=%F :\n",
          x0, y0, r0, x1, y1, r1);
  circle_circle_intersection(x0, y0, r0, x1, y1, r1,
                             &x3, &y3, &x3_prime, &y3_prime);
  printf("  x3=%F, y3=%F, x3_prime=%F, y3_prime=%F\n",
            x3, y3, x3_prime, y3_prime);
            
            
    converter_KilometrosToGraus(x3, &grau,&min,&segundos );
	x3 =  converter_GrausToDecimal(grau,min,segundos);
	
	converter_KilometrosToGraus(y3, &grau,&min,&segundos );
	y3 =  converter_GrausToDecimal(grau,min,segundos);
	
	printf("\n intersecao 1: -%f,-%f\n",x3,y3);
            
    converter_KilometrosToGraus(x3_prime, &grau,&min,&segundos );
	x3 =  converter_GrausToDecimal(grau,min,segundos);
	
	converter_KilometrosToGraus(y3_prime, &grau,&min,&segundos );
	y3 =  converter_GrausToDecimal(grau,min,segundos);
	
	printf("\n intersecao 2: -%f,-%f\n",x3,y3);     
}



int main(void) {
	
	unsigned int grau,min;
	float km1, km2,km3;
	double segundos, x, y, lat, lon;
	float coordenada;
	
	
	double p1_km_x = converter_GrausToKilometros(29,33,47.82); 
	double p1_km_y = converter_GrausToKilometros(50,47,46.20); 
	

	double dec_x1 =  converter_GrausToDecimal(29,33,47.82);	
	double dec_y1 =  converter_GrausToDecimal(50,47,46.20);
	
	LatLonToUTMXY(dec_x1, dec_y1, 0, &x, &y);
	
	printf("utm: %f, %f", x,y);
	
	UTMXYToLatLon (x, y, 0, 1, &lat, &lon);
	
	printf("geo: %f, %f", lat,lon);
	double p2_km_x = converter_GrausToKilometros(29,33,2.41); 
	double p2_km_y = converter_GrausToKilometros(50,47,12.81); 
	
	double p3_km_x = converter_GrausToKilometros(29,34,49.48); 
	double p3_km_y = converter_GrausToKilometros(50,46,28.86); 

	
	run_test(p1_km_x, p1_km_y , 1.26, p2_km_x, p2_km_y, 2.52);
	run_test(p2_km_x, p2_km_y , 2.52, p3_km_x, p3_km_y, 1.98);
	run_test(p1_km_x, p1_km_y , 1.26, p3_km_x, p3_km_y, 1.98);
	
/* PONTO P1 - 	
	 29�33'47.82"S
	  50�47'46.20"O
	  r  = 1,26 km
	  
	PONTO P2 -
	29�33'2.41"S
	50�47'12.81"O
	R = 2,52 km
	
	PONTO P3 - 
	 29�34'49.48"S
	  50�46'28.86"O
	  R = 1,98 KM
	
	
	converter_KilometrosToGraus(2218.36351, &grau,&min,&segundos );
	float dec_x1 =  converter_GrausToDecimal(grau,min,segundos);
	
	converter_KilometrosToGraus(4884.43450, &grau,&min,&segundos );
	float dec_y1 =  converter_GrausToDecimal(grau,min,segundos);
	
	converter_KilometrosToGraus(2219.40363, &grau,&min,&segundos );
	float dec_x2 =  converter_GrausToDecimal(grau,min,segundos);
	
	converter_KilometrosToGraus(4885.12277, &grau,&min,&segundos );
	float dec_y2 =  converter_GrausToDecimal(grau,min,segundos);
	
	
	km1 = fLib_Geo_CalcDistanceBetweenTwoPointsKm( dec_x1, dec_y1, dec_x2, dec_y2 );
	printf("distancia: %f\n",km1);
	
	
	converter_KilometrosToGraus(2218.49622, &grau,&min,&segundos );
	dec_x1 =  converter_GrausToDecimal(grau,min,segundos);
	
	converter_KilometrosToGraus(4885.39437, &grau,&min,&segundos );
	dec_y1 =  converter_GrausToDecimal(grau,min,segundos);
	
	
	km2 = fLib_Geo_CalcDistanceBetweenTwoPointsKm( dec_x2, dec_y2, dec_x1, dec_y1 );
	printf("distancia: %f\n",km2);
	
	
	converter_KilometrosToGraus(2218.36351, &grau,&min,&segundos );
	dec_x2 =  converter_GrausToDecimal(grau,min,segundos);
	
	converter_KilometrosToGraus(4884.43450, &grau,&min,&segundos );
	dec_y2 =  converter_GrausToDecimal(grau,min,segundos);
	
	
	km3 = fLib_Geo_CalcDistanceBetweenTwoPointsKm( dec_x1, dec_y1, dec_x2, dec_y2 );
	printf("distancia: %f\n",km3);
*/	
/*	
	converter_DecimalToGrau(-19.965278, &grau, &min, &segundos);
	coordenada = converter_GrausToDecimal(grau,min,segundos);
	converter_KilometrosToGraus(2218.36351, &grau,&min,&segundos );
*/	
/*	
	double x,y;
	
	teste(2218.36351, 2219.40363, 2218.49622, 4884.43450, 4885.12277, 4885.39437, 0.275, 0.825, 0.825 , &x, &y );
	
	converter_KilometrosToGraus(x, &grau,&min,&segundos );
	coordenada = converter_GrausToDecimal(grau,min,segundos);
	
	printf("%d %d %f\n",grau, min, segundos);
	
	converter_KilometrosToGraus(y, &grau,&min,&segundos );
	coordenada = converter_GrausToDecimal(grau,min,segundos);
	
	printf("%d %d %f\n",grau, min, segundos);
	printf("coordenada y: %f\n",coordenada);
	
	printf("x(km): %f y(km): %f", x, y);
*/	
	
/*	
	
	converter_DecimalToGrau(-29.564409, &grau, &min, &segundos);
	float x1 = converter_GrausToKilometros(grau,min,segundos);
	
	converter_DecimalToGrau(-50.802887, &grau, &min, &segundos);
	float y1 = converter_GrausToKilometros(grau,min,segundos);
	
	converter_DecimalToGrau(-29.562352, &grau, &min, &segundos);
	float x2 = converter_GrausToKilometros(grau,min,segundos);
	
	converter_DecimalToGrau(-50.795110, &grau, &min, &segundos);
	float y2 = converter_GrausToKilometros(grau,min,segundos);
	
	converter_DecimalToGrau(-29.569829, &grau, &min, &segundos);
	float x3 = converter_GrausToKilometros(grau,min,segundos);
	
	converter_DecimalToGrau(-50.796349, &grau, &min, &segundos);
	float y3 = converter_GrausToKilometros(grau,min,segundos);
	
	
	km1 = fLib_Geo_CalcDistanceBetweenTwoPointsKm( -29.564409, -50.802887, -29.562352, -50.795110 );
	km2 = fLib_Geo_CalcDistanceBetweenTwoPointsKm( -29.562352, -50.795110, -29.569829, -50.796349 );
	km3 = fLib_Geo_CalcDistanceBetweenTwoPointsKm( -29.569829, -50.796349, -29.564409, -50.802887 );
	
	float x,y;
	
	teste( x1 , x2, x3, y1, y2, y3, km1/2, km2/2, km3/2 , &x, &y );
	
	converter_KilometrosToGraus(x, &grau,&min,&segundos );
	coordenada = converter_GrausToDecimal(grau,min,segundos);
	
	printf("coordenada x: %f\n",coordenada);
	
	converter_KilometrosToGraus(y, &grau,&min,&segundos );
	coordenada = converter_GrausToDecimal(grau,min,segundos);
	
	printf("coordenada y: %f\n",coordenada);
*/	
/*	
	
	km2 = Calc_CoordenadaMedia( x1,  y1, x2,  y2,  km1 );
	printf("distancia: %f\n",km2);
	
	converter_KilometrosToGraus(km2, &grau,&min,&segundos );
	coordenada =  converter_GrausToDecimal(grau,min,segundos);
	
	printf("Coordenada: %f\n",coordenada);
*/	

/*	
	converter_DecimalToGrau(-43.959917, &grau, &min, &segundos);
	km2 = converter_GrausToKilometros(grau,min,segundos);
	
	printf("Coordenada em km: %f\n",km2);
	
	converter_KilometrosToGraus(km2, &grau,&min,&segundos );
	
	printf("Grau: %d Min: %d Seg: %f\n",grau,min,segundos);
	
 	coordenada =  converter_GrausToDecimal(grau,min,segundos);
	
	printf("Coordenada: %f\n",coordenada);
*/	
	return 0;
}
